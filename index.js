let Company = (function () {
    let counter = 1;
    let registerUserCreateCallback = [];
    let registerUserUpdateCallback = [];
    let registerNoSpaceNotifyer  = [];
    let passwordSuper = Symbol("password superAdmin");
    let users = Symbol("users arr");
    let companyUsers;
    class User {
        #name;
        #lastName;
        #role;
        constructor(name, lastName, isAdmin) {
            this.#name = name;
            this.#lastName = lastName;
            this.id = counter++;
            if (isAdmin === true) {
                let token = Symbol("token");
                this[token] = "secret token";

                   this.createUser = function (name, lastName) {
                    companyUsers = this.company[users];
                    if (this.company.maxSize >= companyUsers.length + 1) {
                        let password = prompt();
                        if (password !== this.company[users][0][passwordSuper]) {
                            alert("Not correct password, not authorized")
                        }
                        if (this[token] !== "secret token") {
                            alert("Not correct token");
                        }
                        let newUser = new User(name, lastName);
                        newUser.#role;
                        console.log(newUser.#role, "newUser.#role")
                            companyUsers.push(newUser);
                            registerUserCreateCallback.forEach(el => el(newUser));
                            if (this.company.maxSize === companyUsers.length) {
                                registerNoSpaceNotifyer.forEach(el =>  el("You added last user!"));
                            }
                            return newUser;
                    }else{
                        console.warn("Too many users")
                    }
                }

                this.deleteUser = function (id) {
                    companyUsers = this.company[users];
                    if (id !== 1) {
                       const [deletedUser] = companyUsers.splice((id - 1), 1);
                        registerUserUpdateCallback.forEach(el => el(deletedUser.id));
                    }
                }
            }

        }

        get dog(){
            return this.#name;
        }
        set dog(newName){
            this.#name = newName;
            registerUserUpdateCallback.forEach(el => el(this));
        }

        get fox(){
            return this.#lastName;
        }
        set fox(newLastName){
            this.#lastName = newLastName;
            registerUserUpdateCallback.forEach(el => el(this));
        }

        // get userRole(){
        //     return newUser.#role;
        // }
        // set userRole(newRole){
        //     newUser.#role = newRole;
        //     registerUserUpdateCallback.forEach(el => el(this));
        // }
    }

    return class Company {
        constructor(companyName, maxSize) {
            this.companyName = companyName;
            this.maxSize = maxSize;
            this[users] = [];
        }
        getUser(index) {
           return  this[users].find(item => item.id === index);
        }
        get curSize(){
            return this[users].length;
        }
        static createSuperAdmin(company) {
            let superAdmin;
            if (!company[users][0]) {
                superAdmin = new User("admin", "admin", true);
                superAdmin.company = company;
                company[users].push(superAdmin);
                superAdmin[passwordSuper] = prompt();
                return superAdmin;
            }
        };
        registerUserCreateCallback(cb) {
            registerUserCreateCallback.push(cb);
        };
        registerUserUpdateCallback(cb) {
            registerUserUpdateCallback.push(cb);
        };
        registerNoSpaceNotifyer(cb) {
            registerNoSpaceNotifyer.push(cb);
        };
    }


})()




